from distutils.core import setup
setup(
    name='newprojectname',#name project
    version='0.0.1',
    packages=['newprojectname'],#name package
    url='url repository',#repository project
    author='autor name',
    author_email='email autor',
    description='description',#description project
    entry_points={
        'console_scripts': [
            'projectname = projectname.cli:main',
        ],
    },
    install_requires=[
        'aiopg>=0.7',
        'aiohttp==0.21.5',
        'aiohttp_session[secure]>=0.4.0',
        'attrdict>=2.0',
        'chilero_pg>=0.3.1',
        'pyyaml>=3.11',
        'schema-migrations>=0.2',
        'pyjwt',
        'cryptography==1.4'
    ]
)