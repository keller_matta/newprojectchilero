import asyncio

from aiohttp.web_reqrep import ContentCoding
from aiohttp_session import get_session

from .utils import User


@asyncio.coroutine
def cors_middleware(app, handler):

    def middleware(request):
        resp = yield from handler(request)
        if resp is not None:
            resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

    return middleware


@asyncio.coroutine
def user_middleware(app, handler):

    def middleware(request):
        request.session = yield from get_session(request)
        token = request.session.get('token', None)

        if token is None:
            auth = request.headers.get('AUTHORIZATION', None)
            if auth is not None and auth.upper().startswith('BEARER '):
                token = auth.split()[1].strip()

        request.auth_token = token
        request.user = User.load(token, app.settings.secret_key)
        resp = yield from handler(request)

        return resp

    return middleware


@asyncio.coroutine
def gzip_middleware(app, handler):

    def middleware(request):
        resp = yield from handler(request)

        resp.enable_compression(ContentCoding.gzip)
        return resp

    return middleware
