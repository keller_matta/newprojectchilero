import chilero.web
import chilero.pg
from .settings import get_settings
from .web import get_middlewares
settings = get_settings()
from aguasanjose import routes

app = chilero.web.init(
    chilero.pg.Application,
    routes,
    settings=settings,
    debug=True,
    middlewares=get_middlewares(settings)
)