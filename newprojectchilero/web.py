import chilero.pg
import chilero.web
from aiohttp_session import session_middleware
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from newprojectname import routes


from newprojectname.middlewares import (cors_middleware, gzip_middleware,
                                    user_middleware)
from newprojectname.settings import get_settings


def get_middlewares(settings):
    cookie_storage = EncryptedCookieStorage(
        settings.secret_key,
        cookie_name='aguasanjose'
    )
    return (
        gzip_middleware,
        session_middleware(cookie_storage),
        user_middleware,
        cors_middleware,
    )
routes = routes


def run_web():  # pragma: no cover
    settings = get_settings()
    chilero.web.run(
        chilero.pg.Application,
        routes,
        settings=settings,
        debug=True,
        middlewares=get_middlewares(settings)
    )


