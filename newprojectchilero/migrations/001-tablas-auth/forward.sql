CREATE TABLE roles (
  id BIGSERIAL PRIMARY KEY,
  nombre TEXT NOT NULL UNIQUE,
  permisos TEXT[],
  fechacreado TIMESTAMP DEFAULT clock_timestamp(),
  fechamodificado TIMESTAMP
);

CREATE TABLE usuarios (
  id BIGSERIAL PRIMARY KEY,
  usuario TEXT NOT NULL UNIQUE,
  clave TEXT NOT NULL,
  roles BIGINT REFERENCES roles(id),
  superadministrador BOOLEAN DEFAULT FALSE,
  activo BOOLEAN DEFAULT TRUE,
  fechacreado TIMESTAMP DEFAULT clock_timestamp(),
  fechamodificado TIMESTAMP,
  nombre text
);
INSERT INTO usuarios(id,usuario,clave,superadministrador) VALUES (0,'root','root', TRUE );
