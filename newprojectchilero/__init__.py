from .resources.usuarios import Usuarios
from .resources.roles import Roles
from .resources.dashboard import Dashboard
from .resources.index import INDEX
routes = [
    ['/usuarios', Usuarios],
    ['/roles', Roles],
    ['/+dashboard', Dashboard],
    ['/login', Usuarios],
    ['/', INDEX]

]