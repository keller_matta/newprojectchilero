import asyncio
import datetime
import decimal
import json
from functools import wraps
from aiohttp import request
from aiohttp.web_exceptions import (
                                    HTTPForbidden)
from chilero.pg.resource import Resource
from chilero.web import Response
from chilero.web.resource import CollectionResponse, EntityResponse


class JSONEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()

        if isinstance(obj, decimal.Decimal):
            return float(obj)

        return None


def permission_required(permission):
    def _dec(f):
        @wraps(f)
        def w(v, *args, **kwargs):
            if v.request.user.has_permission(permission):
                return f(v, *args, **kwargs)

            raise HTTPForbidden(
                headers=(('Access-Control-Allow-Origin', '*'),)
            )

        return w
    return _dec


def resource_permission_required(permission):
    def _dec(f):
        @wraps(f)
        def w(v, *args, **kwargs):
            if not getattr(v, 'has_{}_permission'.format(permission))():
                raise HTTPForbidden(
                    headers=(('Access-Control-Allow-Origin', '*'),))
            return f(v, *args, **kwargs)

        return w
    return _dec


class DBResource(Resource):
    encoder_class = JSONEncoder
    definition=None

    def get_self_url(self):
        resource = self.get_resource_name()
        return self.get_index_url(
            resource,
            **self.default_kwargs_for_urls()) if self.is_collection() \
            else self.get_object_url(
                    self.request.match_info.get('id', None),
                    resource, **self.default_kwargs_for_urls()
                )

    def _build_url(self, args):
        kwarg = self.default_kwargs_for_urls()
        return '{}?{}'.format(
            self.get_index_url(**kwarg),
            '&'.join(
                ['{}={}'.format(k, v) for k, v in args.items()]
            )
        )

    def get_parent(self):
        """Returns the url to the parent endpoint."""
        if self.is_entity():
            return self.get_index_url(**self.default_kwargs_for_urls())
        return super(DBResource, self).get_parent()

    def has_create_permission(self):
        return self.request.user.has_permission('{}.create'.format(self.resource_name))

    @asyncio.coroutine
    def get_usuarioid(self):

        if 'root' == self.request.user.name:
            self.request.user.url = self.get_object_url(0, 'usuarios')
        usuario = yield from self.internal_request(
            'GET', self.request.user.url)
        usuario_json = yield from usuario.json()
        usuarioid = usuario_json['body']['id']
        return usuarioid

    @asyncio.coroutine
    def internal_request(self, method, url, data=None):
        #parsed = urlsplit(url)
        #port = os.getenv('PORT') if 'PORT' in os.environ else parsed.port
        #url = '{}://127.0.0.1:{}{}'.format(parsed.scheme, port, parsed.path)
        headers = self.request.headers.copy()
        if 'CONTENT-TYPE' in headers:
            headers.pop('CONTENT-TYPE')
        if 'CONTENT-LENGTH' in headers:
            headers.pop('CONTENT-LENGTH')
        if 'HOST' in headers:
            headers.pop('HOST')
        if 'ACCEPT-ENCODING' in headers:
            headers.pop('ACCEPT-ENCODING')
        resp = yield from request(
            method, url, headers=headers, data=json.dumps(data))
        return resp

    @asyncio.coroutine
    def get_json(self, url):
        resource = yield from self.internal_request('GET', url)
        resource_json = yield from resource.json()
        return resource_json['body']

    def has_read_permission(self):
        return self.request.user.has_permission('{}.read'.format(self.resource_name))

    def has_update_permission(self):
        return self.request.user.has_permission('{}.update'.format(self.resource_name))

    def has_delete_permission(self):
        return False

    def get_permissions(self):
        return dict(
            create=self.has_create_permission(),
            read=self.has_read_permission(),
            update=self.has_update_permission(),
            delete=self.has_delete_permission(),
            search=bool(self.search_fields)
        )

    def response(self, response, **kwargs):
        extra = dict(
            user_permissions=self.get_permissions()
        )
        if self.is_entity():
            body = response
            if 'extra_content' not in kwargs:
                kwargs['extra_content'] = {}

            kwargs['extra_content'].update(extra)
            return EntityResponse(self, body=body, **kwargs)
        else:
            response.update(extra)
            return CollectionResponse(self, response)

    def collection_options(self, **kwargs):
        return Response(headers=[
            ['Access-Control-Allow-Methods', 'GET, POST, OPTIONS'],
            ['Access-Control-Allow-Origin', '*'],
            ["Access-Control-Allow-Headers",
             "Cookie, Content-Type, Access-Control-Allow-Headers,"
             " Authorization, X-Requested-With"]
        ])

    def entity_options(self, **kwargs):
        return Response(headers=[
            ['Access-Control-Allow-Methods', 'DELETE, GET, PATCH'],
            ['Access-Control-Allow-Origin', '*'],
            ["Access-Control-Allow-Headers",
             "Cookie, Content-Type, Access-Control-Allow-Headers,\
              Authorization, X-Requested-With"]
        ])

    @resource_permission_required('read')
    def index(self, *args, **kwargs):
        return super(DBResource, self).index(*args, **kwargs)

    @resource_permission_required('read')
    def show(self, *args, **kwargs):
        return super(DBResource, self).show(*args, **kwargs)

    @resource_permission_required('create')
    def new(self, **kwargs):
        return super(DBResource, self).new(**kwargs)

    @resource_permission_required('update')
    def update(self, **kwargs):
        return super(DBResource, self).update(**kwargs)

    def fill_data(self, data):
        return data

    def fill_user(self, data):
        if self.definition is not None:
            fields = self.definition['fields']
            if 'usuario' in fields and 'usuario' not in self.required_fields:
                usuarioid = yield from self.get_usuarioid()
                data['idusuario'] = usuarioid
        return data

    def prepare_insert(self, data):
        data = super(DBResource, self).prepare_insert(data)

        data = self.fill_user(data)
        if asyncio.iscoroutine(data):
            data = yield from data

        data = self.fill_data(data)
        if asyncio.iscoroutine(data):
            data = yield from data

        return data

    def prepare_update(self, data):
        data = super(DBResource, self).prepare_update(data)

        data = self.fill_user(data)
        if asyncio.iscoroutine(data):
            data = yield from data

        data = self.fill_data(data)
        if asyncio.iscoroutine(data):
            data = yield from data

        return data

    def exception(self, error_class, error):
        return error_class() if error is None \
            else error_class(body=self.error_response(error),
                             headers=[
                                 ['Access-Control-Allow-Origin', '*'],
                             ]
                             )