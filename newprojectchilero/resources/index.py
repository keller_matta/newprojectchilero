from chilero.web import HTMLResponse, View


class INDEX(View):

    def get(self):
        return HTMLResponse(
            """<html lang="en"><head>
                <meta charset="UTF-8">
                <title>new project name</title>

                <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

                <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/jquery-ui.css">
                <link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css" media="screen,projection">
                <link type="text/css" rel="stylesheet" href="http://code.darwinmonroy.com/user-layer/dev/style.css" media="screen,projection">

                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                </head>
                <body>
                <div id="ul-container"><div class="row" data-reactroot=""><div class="col s6 mundefined offset-s3"><div class="card"><div class="card-content">
                <span class="card-title"></span><div><div class="row">
                <div style=""><img style="width: 90px;" alt="Innovativos SA" src="https://www.google.com/a/innovativos.com.gt/images/logo.gif?alpha=1&amp;service=google_default"></div></div>
                <div class="row">
                <form><div class="input-field col s12"><input value="" id="id_username" type="text">
                <label for="id_username">Usuario</label></div>
                <div class="input-field col s12"><input value="" id="id_password" type="password"><label for="id_password">Clave</label></div><div class="input-field col s12">
                <button class="btn btn-large green right">Conectar</button></div></form></div></div></div></div></div></div></div>
                <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/react/15.2.1/react-dom.min.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.0/external/jquery/jquery.min.js"></script>
                <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
                <script src="http://code.darwinmonroy.com/user-layer/dev/bundle.js"></script>
                <script>
                        if (localStorage=='local'){
                            urlapi='//localhost:8000/';
                        }
                        else{
                            urlapi = ''//localhost:8000/'';
                        }
                        $UL.runners.basic({
                            api: urlapi,
                            lang: 'es',
                            authUrl: 'usuarios/conectar',
                            name: 'nombre de app',
                            logo: 'urllogo',
                            home: '#/usuarios'
                        });

                    </script>

                <div class="hiddendiv common"></div></body></html>"""



        )