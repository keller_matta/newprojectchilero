from newprojectname.resource import DBResource
import datetime


class Roles(DBResource):
    _formulario = [
        {
            'title': 'Rol de usuario',
            'fields': [
                ['nombre'],
                ['permisos']
            ]
        }
    ]
    definition = dict(
        name='rol de usuario',
        name_plural='roles de usuario',
        fields=dict(
            id=dict(type="integer", label="id"),
            nombre=dict(
                type="text", label="nombre", help="nombre del rol"),
            permisos=dict(type='text', help="permisos", widget="multiple"),
            fechacreado=dict(tipo='datetime', etiqueta='fecha creado'),
            fechamodificado=dict(tipo='datetime', etiqueta='fecha modificado')
        ),
        list_columns=['nombre'],
        form_new=_formulario,
        form_edit=_formulario,
        form_view=_formulario
    )
    resource_name = 'roles'
    table_name = 'roles'
    order_by = 'nombre asc'
    allowed_fields = ['nombre', 'permisos']
    search_fields = ['nombre']
    required_fields = ['nombre']

    def serialize_object(self, row):
        return dict(
            id=row[0],
            nombre=row[1],
            permisos=row[2],
            fechacreado=row[3],
            fechamodificado=row[4],
            url=self.get_object_url(row[0])
        )

    def after_serialization(self, data):
        data['_label'] = data['nombre']
        return data

    def prepare_insert(self, data):
        data['fechamodificado'] = datetime.datetime.now()
        return data

    def prepare_update(self, data):
        data['fechamodificado'] = datetime.datetime.now()
        return data
