from chilero.web import View, JSONResponse, Response


class Dashboard(View):
    def options(self):
        return Response(headers=[
            ['Access-Control-Allow-Methods', 'GET'],
            ['Access-Control-Allow-Origin', '*'],
            ["Access-Control-Allow-Headers",
             "Cookie, Content-Type, Access-Control-Allow-Headers,\
              Authorization, X-Requested-With"]
        ])

    def get(self):
        return JSONResponse(
            dict(
                menu=self.load_menu()
            ),
            headers=(
                ('Access-Control-Allow-Origin', '*'),
            )
        )

    def load_menu(self):
        menu = []

        if self.request.user.has_permission('usuarios.read'):
            menu.append(dict(
                link=self.get_full_url(self.app.reverse('usuarios_index')),
                name='Usuarios'
            ))

        if self.request.user.has_permission('roles.read'):
            menu.append(dict(
                link=self.get_full_url(self.app.reverse('roles_index')),
                name='Roles'
            ))

        return menu
