import asyncio
import base64
import datetime
import hashlib

import jwt
from aiohttp.web_exceptions import HTTPBadRequest

from chilero import web
from chilero.web import JSONResponse, View
from newprojectname.resource import DBResource, permission_required


def encode_password(password, salt, secret, iterations=None):
    assert password is not None
    assert salt and '$' not in str(salt)
    if not iterations:
        iterations = 800000
    algorithm = "ocret_pbkdf2_sha256"
    password = bytes(password.encode('utf-8'))
    salt = bytes(salt.encode('utf-8'))
    salt_hash = hashlib.sha256(
        bytes(("{}:{}".format(salt, secret)).encode('utf-8'))
    ).hexdigest()
    dk = hashlib.pbkdf2_hmac(
        'sha256',
        password,
        bytes(salt_hash.encode('utf-8')),
        iterations
    )
    hash = hashlib.sha256(dk).hexdigest()
    return "%s$%d$%s$%s" % (algorithm, iterations, salt_hash, hash)


class Conectar(View):

    def get(self):
        r = yield from self.auth()
        return r

    def post(self):
        r = yield from self.auth()
        return r

    def options(self):
        return JSONResponse({}, headers=(
            ('Access-Control-Allow-Methods', 'GET, POST, OPTIONS'),
            ('Access-Control-Allow-Origin', '*'),
            ("Access-Control-Allow-Headers",
             "Cookie, Content-Type, Access-Control-Allow-Headers,"
             " Authorization, X-Requested-With")
        ))

    @asyncio.coroutine
    def auth(self):
        if 'AUTHORIZATION' in self.request.headers:
            auth = self.request.headers['AUTHORIZATION']
            method, token = auth.split(maxsplit=1)

            userinfo = None
            if method.upper() == 'BASIC':
                data = base64.b64decode(token)
                user, password = data.decode('utf-8').split(':', 1)

                if user == 'root' and bytes(
                   password.encode('utf-8')) == self.app.settings.secret_key:
                    userinfo = dict(
                        url=self.get_full_url('/'),
                        name='root',
                        is_admin=True,
                        permissions=[]
                    )
                else:
                    hashed_password = encode_password(
                        password, user, self.app.settings.secret_key
                    )

                    pool = yield from self.app.get_pool()
                    query = 'select * from usuarios ' \
                        'where usuario=%s and clave=%s and activo=true'
                    with(yield from pool.cursor()) as cur:
                        yield from cur.execute(
                            query,
                            (user, hashed_password)
                        )
                        u = yield from cur.fetchone()
                        if u is not None:
                            userinfo = dict(
                                url=self.get_full_url(
                                    '/usuarios/{}'.format(u[0])),
                                name=u[1],
                                is_admin=u[4],
                                permissions=[]
                            )
                    if u:
                        if u[3]:

                            query2 = 'select permisos from roles where id={}'.format(u[3])
                            with(yield from pool.cursor()) as cur:
                                yield from cur.execute(
                                        query2
                                )
                                r = yield from cur.fetchone()
                                if r[0]:
                                    userinfo['permissions'] = r[0]

                if userinfo is not None:
                    auth_token = jwt.encode(
                        userinfo,
                        self.app.settings.secret_key
                    ).decode('utf-8')

                    self.request.session['token'] = auth_token
                    return JSONResponse(
                        dict(
                            message='Conectado correctamente',
                            token=auth_token,
                            user=userinfo
                        ),
                        headers=(
                            ('Access-Control-Allow-Origin', '*'),
                        )
                    )

        return web.Response(
            status=401,
            headers=(
                ('WWW-Authenticate', 'Basic realm="OCRET"'),
                ('Access-Control-Allow-Origin', '*'),
            )
        )


class Desconectar(View):

    @permission_required('authenticated')
    def get(self):
        self.request.session['token'] = None
        return JSONResponse(dict(message='Desconectado correctamente'))


class Info(View):

    @permission_required('authenticated')
    def get(self):
        return JSONResponse(self.request.user)


class Usuarios(DBResource):

    def has_update_permission(self):
        return True

    def has_read_permission(self):
        return True

    resource_name = 'usuarios'
    nested_collection_resources = dict(
        conectar=Conectar,
        desconectar=Desconectar,
        info=Info
    )

    _formulario = [
        {
            'title': 'Usuario',
            'fields': [
                ['usuario', 'nombre'],
                ['clave', 'confirmarclave'],
                ['superadministrador', 'activo'],
                ['rol']
            ]
        }
    ]
    definition = dict(
        name='usuario',
        name_plural='usuarios',
        fields=dict(
            id=dict(type="integer", label="id"),
            nombre=dict(
                type="text", label="nombre", help="nombre"),
            usuario=dict(
                type="text", label="usuario", help="nombre de usuario"),
            clave=dict(
                type="password", label="clave", help="clave"),
            confirmarclave=dict(
                type="password", label="confirmar clave",
                help="confirmar clave"),
            rol=DBResource.relation('roles', label="rol"),
            rol_name=dict(type='text', label='rol'),
            superadministrador=dict(type="boolean",
                                    label="super administrador"),
            activo=dict(type="boolean", label="activo"),
            fechacreado=dict(type='datetime', label='fecha creado'),
            fechamodificado=dict(type='datetime', label='fecha modificado')
        ),
        list_columns=['nombre', 'usuario', 'superadministrador', 'activo', 'rol_name'],
        form_new=_formulario,
        form_edit=_formulario,
        form_view=_formulario
    )
    order_by = 'usuario asc'
    allowed_fields = [
        'nombre', 'clave', 'activo', 'superadministrador', 'rol',
        'confirmarclave'
    ]
    search_fields = ['usuario']
    required_fields = ['usuario', 'clave']

    def serialize_object(self, row):
        return dict(
            id=row[0],
            usuario=row[1],
            rol=None if row[3] is None else self.get_object_url(
                row[3], 'roles'
            ),
            superadministrador=row[4],
            activo=row[5],
            fechacreado=row[6],
            fechamodificado=row[7],
            nombre=row[8],
            url=self.get_object_url(row[0])
        )

    def after_serialization(self, data):
        data['_label'] = data['usuario'] \
            if data['nombre'] is None else data['nombre']

        if data['rol']:
            roles = yield from self.get_json(data['rol'])
            data['rol_name'] = roles['nombre']

        return data

    @asyncio.coroutine
    def fill_data(self, data):

        if 'rol' in data:
            if data['rol']:
                url = data.pop('rol')
                obj = yield from self.internal_request('GET', url)
                obj_json = yield from obj.json()
                rol = obj_json['body']['id']
                data['roles'] = rol
            else:
                rol = data.pop('rol')
                data['roles'] = rol
        return data

    @asyncio.coroutine
    def prepare_insert(self, data):
        data = yield from self.fill_data(data)
        data['fechamodificado'] = datetime.datetime.now()
        return data

    @asyncio.coroutine
    def prepare_update(self, data):
        data = yield from self.fill_data(data)
        if self.request.user.is_admin or \
           self.request.user.has_permission('usuarios.update'):
            data['fechamodificado'] = datetime.datetime.now()
            return data
        else:
            usuario = yield from self.get_json(self.request.user.url)
            clave = list(data.keys())
            usuarioactual = int(self.request.match_info.get('id'))
            if usuarioactual == usuario['id']:
                if 'clave' in clave:
                    data = {'clave': data['clave']}

                else:
                    raise HTTPBadRequest(
                        body=self.error_response("'Usted unicamente tiene permiso "
                                                 "para modificar su contraseña")
                    )
                return data
            else:
                raise HTTPBadRequest(
                    body=self.error_response("'Usted unicamente tiene permiso "
                                             "para modificar su usuario")
                )

    def validate_allowed_fields(self, data):

        if 'clave' in data:
            clave = data.pop('clave')

            if 'confirmarclave' not in data:
                raise HTTPBadRequest(
                    body=self.error_response("'confirmarclave' no encontrado")
                )
            confirmar = data.pop('confirmarclave')

            if clave != confirmar:
                raise HTTPBadRequest(
                    body=self.error_response("Las contraseñas no coinciden")
                )

            if confirmar is not None and len(confirmar.strip()) > 0:
                data['clave'] = encode_password(
                    clave, data['usuario'], self.app.settings.secret_key
                )

        return data

