import functools
import importlib

import jwt
import jwt.exceptions
from attrdict import AttrDict


def app_loader(app_name):  # pragma: no cover
    return importlib.import_module(app_name)


class AnonymousUser(object):

    @property
    def name(self):
        return 'Anónimo'

    @property
    def is_authenticated(self):
        return False

    def has_permission(self, permission):
        return permission in ['anonymous', 'public']


class User(AttrDict):

    @property
    def is_authenticated(self):
        return True

    def has_permission(self, permission):
        if self.get('is_admin', False) and permission != 'anonymous':
            return True

        if permission in ['public', 'authenticated']:
            return True

        return permission in self.permissions

    def __init__(self, secret, token):
        data = self.decode(secret, token)
        assert 'name' in data
        super(User, self).__init__(data)

    def decode(self, secret, token):
        return jwt.decode(token, secret, verify=True, algorithms=['HS256'])

    def encode(self, secret):
        return jwt.encode(self, secret, algorithm='HS256')

    @staticmethod
    @functools.lru_cache()
    def load(token, secret):
        try:
            return User(secret, token)
        except:
            pass
