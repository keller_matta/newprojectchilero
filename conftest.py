from projectname.db import drop_database, create_database
from projectname.settings import get_settings

def pytest_configure():
    settings = get_settings()
    drop_database()
    create_database()
